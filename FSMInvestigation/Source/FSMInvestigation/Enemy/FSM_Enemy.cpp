// Fill out your copyright notice in the Description page of Project Settings.


#include "FSM_Enemy.h"

#include "Components/StaticMeshComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "Kismet\KismetMathLibrary.h"

#include "FSMInvestigationCharacter.h"

// Sets default values
AFSM_Enemy::AFSM_Enemy()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	// Setup mesh component with collision
	EnemyMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("EnemyMesh"));
	EnemyMesh->BodyInstance.SetCollisionProfileName("PhysicsActor");
	EnemyMesh->SetupAttachment(RootComponent);
	EnemyMesh->OnComponentHit.AddDynamic(this, &AFSM_Enemy::OnHit);

	// If the mesh was found, set it
	ConstructorHelpers::FObjectFinder<UStaticMesh> CowAsset(TEXT("StaticMesh'/Game/Input_Examples/Meshes/SM_Toy_Cow.SM_Toy_Cow'"));
	if (CowAsset.Succeeded())
		EnemyMesh->SetStaticMesh(CowAsset.Object);
}

// Called when the game starts or when spawned
void AFSM_Enemy::BeginPlay()
{
	Super::BeginPlay();
	
	// Store original patrol start and end vectors on BeginPlay
	m_patrolFromVector = this->GetActorLocation();
	m_patrolToVector = m_patrolFromVector + (this->GetActorForwardVector() * m_patrolDistance);

	// Grab reference to player on start
	m_player = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);

	if (!m_player)
		UE_LOG(LogTemp, Error, TEXT("Unable to find player!"));
}

// Called every frame
void AFSM_Enemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Debug Main Line to player
	//DrawDebugLine(GetWorld(), thisLoc, targetLoc, FColor(128, 0, 0), false, -1, 0, 2);

	//if (State == BehaviourStates::IDLE || State == BehaviourStates::PATROL || State == BehaviourStates::ALERT)
	if(State != BehaviourStates::DEATH)
	{
		//Draw debug field of view
		FVector offset = this->GetActorLocation() + FVector(0.0f, 0.0f, 50.0f);
		UKismetSystemLibrary::DrawDebugConeInDegrees(GetWorld(), offset, this->GetActorForwardVector(), VisibilityRange, VisibilityFoV / 2, 10.0f, 12, FLinearColor::Red, 0.0f, 0.0f);
	}

	FSMUpdate(DeltaTime);
}

// Hit event.
void AFSM_Enemy::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (m_isDead)
		return;

	// Check if collision was made by player character
	if (OtherActor != nullptr && OtherActor->GetClass()->IsChildOf(AFSMInvestigationCharacter::StaticClass()))
	{
		m_isDead = true;
	}
}

void AFSM_Enemy::FSMUpdate(float DeltaTime)
{
	if (State == BehaviourStates::IDLE)
	{
		if (Event == GameEvents::ON_ENTER)
			IdleEnter();
		else if (Event == GameEvents::ON_UPDATE)
			IdleUpdate(DeltaTime);
	}

	if (State == BehaviourStates::PATROL)
	{
		if (Event == GameEvents::ON_ENTER)
			PatrolEnter();
		else if (Event == GameEvents::ON_UPDATE)
			PatrolUpdate();
	}

	if (State == BehaviourStates::ALERT)
	{
		if (Event == GameEvents::ON_ENTER)
			AlertEnter();
		else if (Event == GameEvents::ON_UPDATE)
			AlertUpdate(DeltaTime);
	}

	if (State == BehaviourStates::ENGAGE)
	{
		if (Event == GameEvents::ON_ENTER)
			EngageEnter();
		else if (Event == GameEvents::ON_UPDATE)
			EngageUpdate(DeltaTime);
	}

	if (State == BehaviourStates::DEATH)
	{
		if (Event == GameEvents::ON_ENTER)
			DeathEnter();
		else if (Event == GameEvents::ON_UPDATE)
			DeathUpdate();
	}
}

void AFSM_Enemy::ChangeBehaviorState(BehaviourStates newState)
{
	State = newState;
	Event = GameEvents::ON_ENTER;
}

void AFSM_Enemy::IdleEnter()
{
	Event = GameEvents::ON_UPDATE;
}

void AFSM_Enemy::IdleUpdate(float DeltaTime)
{
	// Check if player comes within sight whilst idling
	if (IsPlayerInSight())
	{
		UE_LOG(LogTemp, Log, TEXT("Can see player while idle! Moving to alert"));
		ChangeBehaviorState(BehaviourStates::ALERT);
	}

	// Killed by player whilst idling
	if (m_isDead)
	{
		UE_LOG(LogTemp, Log, TEXT("Killed by player whilst idle. Killing enemy..."));
		ChangeBehaviorState(BehaviourStates::DEATH);
	}

	// Increment idle time and continue patrolling
	m_idleTime += DeltaTime;
	if (m_idleTime >= m_maxIdleWaitTime) 
	{
		ChangeBehaviorState(BehaviourStates::PATROL);
		UE_LOG(LogTemp, Log, TEXT("Finished Idle Wait - Patroling..."));
		m_idleTime = 0.0f;
	}
}

void AFSM_Enemy::PatrolEnter()
{
	Event = GameEvents::ON_UPDATE;
}

void AFSM_Enemy::PatrolUpdate()
{
	FVector currentLocation = this->GetActorLocation();

	// Check if the enemy has a target to walk to and assign a new target if so
	if (!m_isPatroling)
	{
		float fromLocDist = FVector::DistSquared(currentLocation, m_patrolFromVector);
		float toLocDist = FVector::DistSquared(currentLocation, m_patrolToVector);

		// Check if enemy is on top/near a patrol target
		if (fromLocDist < m_patrolTargetTolerance) {
			m_patrolTarget = m_patrolToVector;
		}
		else if (toLocDist < m_patrolTargetTolerance) 
		{
			m_patrolTarget = m_patrolFromVector;
		}
		else if (fromLocDist < toLocDist)
		{
			//From Vector is closer
			m_patrolTarget = m_patrolFromVector;
		}
		else if (toLocDist < fromLocDist)
		{
			// To Vector is closer
			m_patrolTarget = m_patrolToVector;
		}
	}

	// Change to Idle when reached current patrol target
	float distanceToTarget = FVector::DistSquared(currentLocation, m_patrolTarget);
	if (distanceToTarget <= m_patrolTargetTolerance)
	{
		m_isPatroling = false;
		m_patrolTarget = FVector::ZeroVector;

		UE_LOG(LogTemp, Log, TEXT("Reached target, idling..."));
		ChangeBehaviorState(BehaviourStates::IDLE);
	}

	// If enemy has a target, walk to that patrol target
	if (m_patrolTarget != FVector::ZeroVector)
	{
		m_isPatroling = true;

		FVector vectorToTarget = m_patrolTarget - currentLocation;
		vectorToTarget.Normalize();
		vectorToTarget.Z = 0;
		vectorToTarget *= PatrolSpeed;

		// Update position and look at target
		this->SetActorLocation(currentLocation + vectorToTarget);
		LookAtTarget(m_patrolTarget);
	}

	// Check is the player is within enemy's vision cone
	if (IsPlayerInSight())
	{
		UE_LOG(LogTemp, Log, TEXT("Can see player! Moving to alert"));
		ChangeBehaviorState(BehaviourStates::ALERT);
	}

	// Killed by player whilst patrolling
	if (m_isDead)
	{
		UE_LOG(LogTemp, Log, TEXT("Killed by player whilst patrolling. Killing enemy..."));
		ChangeBehaviorState(BehaviourStates::DEATH);
	}
}

void AFSM_Enemy::AlertEnter()
{
	Event = GameEvents::ON_UPDATE;

	// Store inital player position when alerted
	m_alertPlayerPos = m_player->GetActorLocation();
}

void AFSM_Enemy::AlertUpdate(float DeltaTime)
{
	// Increment alert duration every tick
	m_alertDuration += DeltaTime;

	// Increment in sight duration if player remains in sight for too long
	if (IsPlayerInSight()) 
	{
		m_inSightDuration += DeltaTime;
		m_alertPlayerPos = m_player->GetActorLocation();
	}

	// Set direction to look at alert location
	LookAtTarget(m_alertPlayerPos);

	// Draw debug point of alert location
	DrawDebugPoint(GetWorld(), m_alertPlayerPos, 15, FColor(0, 0, 128), false, 0.03);

	// If player is in sight for too long, engage the player
	if (m_inSightDuration >= m_maxInSightDuration)
	{
		UE_LOG(LogTemp, Log, TEXT("Player in sight for '%f' seconds, Engaging!"), m_maxInSightDuration);
		ChangeBehaviorState(BehaviourStates::ENGAGE);

		m_inSightDuration = 0.0f;
	}

	// Only be alerted for certain amount of time
	// Check if time is more than max duration
	if (m_alertDuration >= m_maxAlertDuration)
	{
		UE_LOG(LogTemp, Log, TEXT("Alert without detecting player has expired, move to patrol..."));
		ChangeBehaviorState(BehaviourStates::PATROL);
		m_alertDuration = 0.0f;
		m_alertPlayerPos = FVector::ZeroVector;
	}

	// Killed by player whilst in alert state
	if (m_isDead)
	{
		UE_LOG(LogTemp, Log, TEXT("Killed by player whilst alerted. Killing enemy..."));
		ChangeBehaviorState(BehaviourStates::DEATH);
	}
}

void AFSM_Enemy::EngageEnter()
{
	Event = GameEvents::ON_UPDATE;
}

void AFSM_Enemy::EngageUpdate(float DeltaTime)
{
	if (IsPlayerInSight())
	{
		m_engageOutOfSightDuration = 0.0f;

		// Check distance enemy is away from player and move toward, stopping before getting too close
		float dist = FVector::DistSquared(this->GetActorLocation(), m_player->GetActorLocation());
		if (dist > m_engageDistance)
		{
			FVector playerVector = (m_player->GetActorLocation()) - this->GetActorLocation();
			playerVector.Normalize();
			playerVector.Z = 0;
			playerVector *= m_engageMoveSpeed;

			// Move location and rotation at player
			SetActorLocation(this->GetActorLocation() + playerVector, true);
			LookAtTarget(m_player->GetActorLocation());
		}
	}
	else
	{
		// Increment out of sight duration if player isn't in sight
		m_engageOutOfSightDuration += DeltaTime;
	}

	/// Once player is outside of sight for too long, return to patrolling
	if (m_engageOutOfSightDuration >= m_maxEngageOutOfSightDuration)
	{
		UE_LOG(LogTemp, Log, TEXT("Lost vision of player. Patrolling..."));
		ChangeBehaviorState(BehaviourStates::PATROL);
		m_engageOutOfSightDuration = 0.0f;
	}

	/// Killed by player whilst engaging
	if (m_isDead)
	{
		UE_LOG(LogTemp, Log, TEXT("Killed by player whilst engaging. Killing enemy..."));
		ChangeBehaviorState(BehaviourStates::DEATH);
	}
}

void AFSM_Enemy::DeathEnter()
{
	Event = GameEvents::ON_UPDATE;

	/// Pitch up to represent death
	FRotator rotation = this->GetActorRotation();
	rotation.Pitch = 90.0f;
	this->SetActorRotation(rotation);
}

void AFSM_Enemy::DeathUpdate()
{
	
}

float AFSM_Enemy::GetAngleOfPlayer()
{
	// Get forward facing vector
	FVector forwardVector = this->GetActorForwardVector();
	forwardVector.Normalize();

	FVector playerVector = m_player->GetActorLocation();
	FVector thisVector = this->GetActorLocation();
	playerVector.Z = 0;
	thisVector.Z = 0;

	// Get facing vector to player
	FVector target = playerVector - thisVector;
	target.Normalize();

	/// Calculate sin and cos, or Y and X
	double sin = forwardVector.X * target.Y - target.X * forwardVector.Y;
	double cos = forwardVector.X * target.X + forwardVector.Y * target.Y;

	/// Find result and convert to be between -180 and +180
	return FMath::Atan2(sin, cos) * (180 / UKismetMathLibrary::GetPI());
}

bool AFSM_Enemy::IsPlayerInSight()
{
	// Get the distance in units player is away from location
	float distance = FVector::Dist(this->GetActorLocation(), m_player->GetActorLocation());

	// Get angle that player is away from enemy
	float angle = GetAngleOfPlayer();
	float halfFoVCone = VisibilityFoV / 2;

	// Distance is within visible range and is within visibility cone
	return distance <= VisibilityRange && (angle < halfFoVCone && angle > -halfFoVCone);
}

void AFSM_Enemy::LookAtTarget(FVector target)
{
	FRotator lookAtRotator = UKismetMathLibrary::FindLookAtRotation(this->GetActorLocation(), target);
	FRotator currentRotation = this->GetActorRotation();
	currentRotation.Yaw = lookAtRotator.Yaw;
	SetActorRotation(currentRotation);
}
