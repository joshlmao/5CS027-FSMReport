// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FSM_Enemy.generated.h"

UCLASS()
class FSMINVESTIGATION_API AFSM_Enemy : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFSM_Enemy();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	/// All behaviour states of the enemy
	enum BehaviourStates { IDLE, PATROL, ALERT, ENGAGE, DEATH };
	/// Current behaviour state of the enemy
	BehaviourStates State = BehaviourStates::IDLE;

	/// State machine entry events
	enum GameEvents { ON_ENTER, ON_UPDATE };
	/// Current entry state of the FSM
	GameEvents Event = GameEvents::ON_ENTER;

	/// Speed to walk at when patrolling
	float PatrolSpeed = 2.0f;
	float PatrolDistance = 1000.0f;
	/// Range the enemy can detect the player in units
	float VisibilityRange = 650.0f;
	/// Degrees of field of view of enemy
	float VisibilityFoV = 110.0f;

	void FSMUpdate(float DeltaTime);

	/// Set the newest behaviour state of the enemy
	void ChangeBehaviorState(BehaviourStates newState);

	/* Idle State functions */
	void IdleEnter();
	void IdleUpdate(float DeltaTime);

	/* Patrol State functions */
	void PatrolEnter();
	void PatrolUpdate();

	/* Alert State functions */
	void AlertEnter();
	void AlertUpdate(float DeltaTime);

	/* Engage State functions */
	void EngageEnter();
	void EngageUpdate(float DeltaTime);

	/* Death State functions */
	void DeathEnter();
	void DeathUpdate();

private:
	class UStaticMeshComponent* EnemyMesh;
	/// Pointer to the current player
	class APawn* m_player;

	/* Idle Variables */
	/// Current time of waiting idle
	float m_idleTime = 0.0f;
	/// Max idle time before moving to patrolling
	float m_maxIdleWaitTime = 5.0f;

	/* Patrol Variables */
	/// Start vector that enemy will patrol from
	FVector m_patrolFromVector;
	/// End vector that enemy will patrol to
	FVector m_patrolToVector;
	/// Current patrol target the enemy will walk to
	FVector m_patrolTarget = FVector::ZeroVector;
	/// Is the enemy currently patroling
	bool m_isPatroling;

	/// Distance amount to add from their current position and direction to patrol
	float m_patrolDistance = 1000.0f;
	/// Amount of distance away from a patrol target to stop and consider them to have reached the target
	float m_patrolTargetTolerance = 5.0f;

	/* Alert variables */
	/// Vector of player within the vision cone
	FVector m_alertPlayerPos;
	/// Current duration of being in alert
	float m_alertDuration = 0.0f;
	/// Maximum duration of being in alert before returning to patrolling
	float m_maxAlertDuration = 10.0f;
	/// Current duration player has been in sight of enemy
	float m_inSightDuration = 0.0f;
	/// Max duration player is in sight before moving to Engage state
	float m_maxInSightDuration = 3.0f;

	/* Engage variables */
	/// Current duration player is missing
	float m_engageOutOfSightDuration = 0.0f;
	/// Duration of player being outside of vision before moving back to patrolling
	float m_maxEngageOutOfSightDuration = 7.0f;
	/// Move speed the move towards the target at
	float m_engageMoveSpeed = 2.0f;
	/// Amount of distance from player to stop moving toward player
	float m_engageDistance = 100.0f;

	/* Death variables */
	/// Is the enemy currently dead
	bool m_isDead = false;

	/// Gets the angle of the player from the current enemy's position
	// Returns value between negative 180 to 180
	float GetAngleOfPlayer();
	/// Checks if the player is within the visiblity cone
	bool IsPlayerInSight();
	/// Sets the current rotation to look towards the destined vector
	void LookAtTarget(FVector target);
};
