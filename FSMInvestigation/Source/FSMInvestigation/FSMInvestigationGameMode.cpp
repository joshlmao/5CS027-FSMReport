// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "FSMInvestigationGameMode.h"
#include "FSMInvestigationPlayerController.h"
#include "FSMInvestigationCharacter.h"
#include "UObject/ConstructorHelpers.h"

AFSMInvestigationGameMode::AFSMInvestigationGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AFSMInvestigationPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}